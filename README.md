# Ansible Role: stagecoach

* Installs stagecoach and forever

## Requirements

  CentOS 7

## Role Variables

Linux user for isleward
```
isleward_user: isleward
```

Stagecoach repo address
```
sc_repo_address: https://gitlab.com/Isleward/ansible/stagecoach.git
```

## Dependencies

  None.

## Example Playbook
````
- hosts: localhost
  roles:
    - isleward.stagecoach
````  

## Author Information

This role was created in 2018 by Vildravn.